<?php
/**
 * @file
 * Contains \Drupal\security_questions\Form\SecurityQuestionsListForm.
 */

namespace Drupal\security_questions\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements an SecurityQuestionsList form.
 */
class SecurityQuestionsListForm extends FormBase {

  /**
  * {@inheridoc}
  */
  protected function getEditableConfigNames() {
    return array('security_questions.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'security_questions_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
   $form = array();

   // Add a new global question.
   $form['add'] = array('#type' => 'container');
   $form['add']['question'] = array(
     '#title' => $this->t('Add a question'),
     '#type' => 'textfield',
     '#description' => $this->t('Enter the question text.'),
   );
   $form['add']['actions'] = array('#type' => 'actions');
   $form['add']['actions']['submit'] = array(
     '#type' => 'submit',
     '#value' => $this->t('Add'),
   );

   // List global questions.
   $form['list'] = array('#type' => 'container');
   $form['list'][] = array(
     '#prefix' => '<h3>',
     '#markup' => $this->t('Global questions'),
     '#suffix' => '</h3>',
   );
   $form['list'][] = array(
     '#prefix' => '<p>',
     '#markup' => $this->t('The questions shown below are available for use by all users. Optionally, users may also be allowed to create their own questions.'),
     '#suffix' => '</p>',
   );

   $rows = array();
   $questions = security_questions_question_load_multiple(array('uid' => 0));
   foreach ($questions as $question) {
     $rows[] = array(check_plain($question->question), check_plain($question->machine_name), l($this->t('delete'), 'admin/config/people/security_questions/questions/delete/' . $question->sqid));
   }
   $table = array(
     'header' => array($this->t('Question'), $this->t('Machine name'), $this->t('Operations')),
     'rows' => $rows,
     'empty' => $this->t('No global questions available. Use the form above to create one.'),
   );

   // See if any other modules want to add anything to the table.
   \Drupal::moduleHandler()->alter('security_questions_list', $table);
   $output = drupal_render($table);
   $pager = array('#theme' => 'pager');
   $output .= drupal_render($pager);
   $form['list'][] = array('#markup' => $output);

   return $form;
  }


 /**
  * {@inheritdoc}
  */
 public function validateForm(array &$form, FormStateInterface $form_state) {
   $question = trim($form_state['values']['question']);
   if (empty($question)) {
     $form_state->setErrorByName('question', $this->t('The question text must not be blank.'));
   }
 }
 /**
  * Questions add form submit handler.
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $number_required = $form_state->getValue('question');
 }
}
