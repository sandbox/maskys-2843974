<?php
/**
* @file
* Contains \Drupal\security_questions\Form\SecurityQuestionsSettingsForm.php
*/
namespace Drupal\security_questions\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
class SecurityQuestionsSettingsForm extends FormBase {
  /**
  * {@inheridoc}
  */
  protected function getEditableConfigNames() {
    return array('security_questions.settings');
  }

  /**
  * {@inheridoc}
  */
  public function getFormId() {
    return 'security_questions.settings';
  }
/**
 * Security questions module settings form.
 */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('security_questions.settings');
    $form['security_questions_number_required'] = array(
      '#title' => $this->t('Required number of questions'),
      '#description' => $this->t('The number of questions that users are required to have answered on their accounts.'),
      '#type' => 'select',
      '#default_value' => $config->get('security_questions_number_required', 3),
      '#options' => array_combine(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10),array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
    );
    $form['security_questions_user_questions'] = array(
      '#title' => $this->t('User-defined questions'),
      '#description' => $this->t('Allow users to create their own questions to answer.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('security_questions_user_questions', FALSE),
    );
    // Configure question challenges.
    $form['challenges'] = array(
      '#title' => $this->t('Protected forms'),
      '#description' => $this->t('Users will be required to answer a security question when submitting one of the forms selected below.'),
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
    );
    $form['challenges']['security_questions_password_reset'] = array(
      '#title' => $this->t('Password reset request'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('security_questions_password_reset', FALSE),
    );
    $form['challenges']['security_questions_user_login'] = array(
      '#title' => $this->t('User login'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('security_questions_user_login', FALSE),
    );
    $form['challenges']['security_questions_protection_mode'] = array(
      '#title' => $this->t('When should the question challenge occur during login?'),
      '#description' => $this->t("Note that if the question is asked before the password, malicious people may be able to guess a user's answer through social engineering, just by knowing the user's username."),
      '#type' => 'radios',
      '#default_value' => $config->get('security_questions_protection_mode', 'after'),
      '#options' => array(
        'after' => $this->t('After entering both the username and the password'),
        'before' => $this->t('After entering the username, but before the password'),
      ),
      '#states' => array(
        'visible' => array(
          ':input[name="security_questions_user_login"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['challenges']['security_questions_cookie'] = array(
      '#title' => $this->t('Show "remember this computer" option on protected forms'),
      '#description' => $this->t('If enabled, users will have the option to disable future question challenges for actions performed from the same computer.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('security_questions_cookie', FALSE),
    );
    $form['challenges']['security_questions_cookie_expire'] = array(
      '#title' => $this->t('Cookies expire after'),
      '#description' => $this->t('Controls how long a user is allowed to bypass question challenges after selecting the "remember this computer" option.'),
      '#type' => 'select',
      '#default_value' => $config->get('security_questions_cookie_expire', 604800),
      '#options' =>array_combine(array(0 => $this->t('Never'),86400, 172800, 259200, 432000, 604800, 1209600, 2419200, 7776000),array(0 => $this->t('Never'),86400, 172800, 259200, 432000, 604800, 1209600, 2419200, 7776000)),
      '#states' => array(
        'visible' => array(
          ':input[name="security_questions_cookie"]' => array('checked' => TRUE),
        ),
      ),
    );
    // Configure flood control.
    $form['flood'] = array(
      '#title' => $this->t('Flood control'),
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
    );
    $form['flood']['security_questions_flood_expire'] = array(
      '#title' => $this->t('After giving an incorrect answer, for how long should a visitor be blocked from attempting that same question again?'),
      '#description' => $this->t("When a question is answered incorrectly, the visitor's IP address will be logged in the database, along with the question that was asked and the ID of the user account to which the visitor was trying to authenticate.  The logged question will not be shown again to that IP for that user account until either the timeout period set here expires or the visitor answers a question correctly for that user account.  If the visitor answers all of the user's questions incorrectly, access as that user will be denied to that IP for anything protected by security questions until the timeout expires for one or more questions."),
      '#type' => 'select',
      '#default_value' => $config->get('security_questions_flood_expire', 3600),
      '#options' =>array_combine(array(0 => $this->t('No delay'),60, 300, 900, 1800, 3600, 10800, 21600, 43200, 86400, 604800),array(0 => $this->t('No delay'),60, 300, 900, 1800, 3600, 10800, 21600, 43200, 86400, 604800))
    );
    return parent::buildForm($form, $form_state);
  }
/**
  * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $number_required = $form_state->getValue('security_questions_number_required');
    $user_questions = $form_state->getValue('security_questions_user_questions');
    $password_reset = $form_state->getValue('security_questions_password_reset');
    $user_login = $form_state->getValue('security_questions_user_login');
    $protection_mode = $form_state->getValue('security_questions_protection_mode');
    $cookie = $form_state->getValue('security_questions_cookie');
    $cookie_expire = $form_state->getValue('security_questions_cookie_expire');
    $flood_expire = $form_state->getValue('security_questions_flood_expire');
    // Get the config object.
    $config = $this->config('security_questions.settings');
    // Set the values the user submitted in the form
    $config->set('number_required', $number_required)
        ->set('user_questions', $user_questions)
        ->set('password_reset', $password_reset)
        ->set('user_login', $user_login)
        ->set('protection_mode', $protection_mode)
        ->set('cookie', $cookie)
        ->set('cookie_expire', $cookie_expire)
        ->set('flood_expire', $flood_expire)
        ->save();
  }
}
