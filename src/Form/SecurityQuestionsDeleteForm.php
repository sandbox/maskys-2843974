<?php
namespace Drupal\security_questions\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Question delete form.
 *
 * @param $question
 *   A question object.
 */
class SecurityQuestionsDeleteForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
      return array('security_questions.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'security_questions_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

  $count = db_select('security_questions_answers', 'a')
    ->condition('sqid', $question->sqid)
    ->countQuery()
    ->execute()
    ->fetchField();
  $tokens = array('%question' => $question->question);

  $form['sqid'] = array('#type' => 'hidden', '#value' => $question->sqid);
  $form['question'] = array('#type' => 'value', '#value' => $question->question);
  $confirm = t('Are you sure you want to delete %question from the global security questions list?', $tokens);
  $path = 'admin/config/people/security_questions';
  $description = format_plural($count,
    '%question is currently being used by 1 user.',
    '%question is currently being used by @count users.',
    $tokens);

  return parent::buildForm($form, $form_state);

    security_questions_question_delete($form_state['values']['sqid']);

    $form_state['redirect'] = 'admin/config/people/security_questions';
    drupal_set_message(t('The security question %question has been deleted.', array('%question' => $form_state['values']['question'])));
 }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }
}
